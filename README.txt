The Test Anything Protocol (testanything.org) is a format to represent test results.

This is a small plugin that adds syntax highlighting with folding for TAP files,
This works automatically for files with the .tap extension.
