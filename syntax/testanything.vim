" This file is part of vim-testanything.
"
" vim-testanything is free software: you can redistribute it and/or modify it under the
" terms of the GNU General Public License as published by the Free Software
" Foundation, either version 3 of the License, or (at your option) any later
" version.
"
" vim-testanything is distributed in the hope that it will be useful, but WITHOUT ANY
" WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
" A PARTICULAR PURPOSE. See the GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License along with
" vim-testanything. If not, see <https://www.gnu.org/licenses/>.
if exists("b:current_syntax")
  finish
endif

" order matters here, so be careful

" match foldable region with all lines between test points
syntax region testData start=/^\(#\|\s\)/ end=/^\ze\(ok\|not ok\)/ fold contains=yamlmark

syntax match yamlmark /  ----$/ contained

syntax match ok /^ok/
syntax match notok /^not ok/
syntax match bail /^Bail Out!.*$/

" These need to override (not ok) test points
syntax match dirtodo /^.*# TODO/
syntax match dirskip /^.*# SKIP/

hi def link testData Comment
hi def link ok Special
hi def link notok Error
hi def link bail Error
hi def link dirtodo Special
hi def link dirskip Special
hi def link yamlmark Keyword
