" This file is part of vim-testanything.
"
" vim-testanything is free software: you can redistribute it and/or modify it under the
" terms of the GNU General Public License as published by the Free Software
" Foundation, either version 3 of the License, or (at your option) any later
" version.
"
" vim-testanything is distributed in the hope that it will be useful, but WITHOUT ANY
" WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
" A PARTICULAR PURPOSE. See the GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License along with
" vim-testanything. If not, see <https://www.gnu.org/licenses/>.
if exists('b:did_ftplugin')
    finish
endif
let b:did_ftplugin = 1



setlocal foldminlines=3
setlocal foldcolumn=2
setlocal foldenable
setlocal foldmethod=syntax
